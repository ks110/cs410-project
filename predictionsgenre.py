import pandas as pd
import matplotlib.pyplot as plt

data = pd.read_csv('test_with_predictions_svm_updated.csv')

genre_expected = data.groupby('Expected')['Genre'].value_counts().unstack().fillna(0)
genre_predicted = data.groupby('Predicted')['Genre'].value_counts().unstack().fillna(0)

fig, axes = plt.subplots(1, 2, figsize=(12, 6))

# Plot for Genre with Expected and Predicted
genre_expected.plot(kind='bar', stacked=True, ax=axes[0])
axes[0].set_title('Genre with Expected')
axes[0].set_ylabel('Count')
axes[0].legend(title='Expected')

genre_predicted.plot(kind='bar', stacked=True, ax=axes[1])
axes[1].set_title('Genre with Predicted')
axes[1].set_ylabel('Count')
axes[1].legend(title='Predicted')

plt.tight_layout()
plt.show()
