import pandas as pd

df = pd.read_csv('criticreviews.csv', encoding='utf-8')

column_index = 3

column_six = df.iloc[:, column_index]

column_six.to_csv('descriptionreviewsonly.csv', index=False, header=['Description'], encoding='utf-8')

print("The reviews column extracted and saved to 'descriptionreviewsonly.csv'")
