import pandas as pd
import nltk
import re
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.sentiment.vader import SentimentIntensityAnalyzer

nltk.download('punkt')
nltk.download('stopwords')
nltk.download('vader_lexicon')

df = pd.read_csv('descriptionreviewsonly.csv') 
column_index = 0  

stop_words = set(stopwords.words('english'))

sid = SentimentIntensityAnalyzer()

def preprocess_and_sentiment(description):
    words = word_tokenize(str(description))
    
    filtered_words = [word.lower() for word in words if word.lower() not in stop_words and re.match('^[a-zA-Z0-9_]+$', word)]
    
    filtered_sentence = ' '.join(filtered_words)
    
    return sid.polarity_scores(filtered_sentence)['compound']

df['SentimentScore'] = df.iloc[:, column_index].apply(preprocess_and_sentiment)

positive_threshold = 0.1
negative_threshold = -0.1

df['Sentiment'] = df['SentimentScore'].apply(lambda score: 'Positive' if score > positive_threshold else ('Negative' if score < negative_threshold else 'Neutral'))

df.to_csv('sentimentondescriptionreviewsonly.csv', index=False)

print("Sentiment analysis applied after removing stopwords and saved")
