import pandas as pd

df_float_ratings = pd.read_csv('criticreviews.csv')

df_sentiment = pd.read_csv('sentimentondescriptionreviewsonly.csv')

merged_df = pd.concat([df_float_ratings, df_sentiment[['SentimentScore', 'Sentiment']]], axis=1)

merged_df.to_csv('criticsreviewwithsentiment.csv', index=False)

print("Merged dataset saved ")
