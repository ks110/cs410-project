import pandas as pd
import nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from sklearn.calibration import LabelEncoder
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score

# Download NLTK resources if not downloaded already
nltk.download('punkt')
nltk.download('stopwords')

# Function to preprocess the text data
def preprocess_text(text):
    tokens = word_tokenize(text.lower())  # Tokenize and convert text to lowercase
    
    # Remove stopwords and non-alphanumeric tokens
    stop_words = set(stopwords.words('english'))
    filtered_tokens = [word for word in tokens if word.isalnum() and word not in stop_words]
    
    return ' '.join(filtered_tokens)

# Load the CSV file into a pandas DataFrame
# Replace 'criticsreviewwithsentiment.csv' with your actual file path
df = pd.read_csv('criticsreviewwithsentiment.csv', encoding='utf-8')

# Preprocess the text data
df['preprocessed_summary'] = df['summary'].fillna('').apply(preprocess_text)

# Split the dataset into training and testing sets
X = df['preprocessed_summary']
y = df['Sentiment']

# Convert text data to numerical feature vectors using TF-IDF
tfidf_vectorizer = TfidfVectorizer()
X_tfidf = tfidf_vectorizer.fit_transform(X)

# Train the Support Vector Machine (SVM) classifier
svm_classifier = SVC(kernel='linear')
svm_classifier.fit(X_tfidf, y)

# Example predictions
data = pd.read_csv("test.csv")
test_data = data['Review'].fillna('').tolist()

predicted_sentiments = []  # List to store predicted sentiments

for review in test_data:
    preprocessed_review = preprocess_text(review)
    review_tfidf = tfidf_vectorizer.transform([preprocessed_review])
    prediction = svm_classifier.predict(review_tfidf)
    predicted_sentiments.append(prediction[0])  # Append each prediction to the list
    print(f"Review: {review} | Predicted Sentiment: {prediction[0]}")

# Add predicted sentiments to the DataFrame
data['Predicted'] = predicted_sentiments
data.to_csv('test_with_predictions_svm.csv', index=False, encoding='utf-8')

data['Expected'] = data['Expected'].astype(str)
data['Predicted'] = data['Predicted'].astype(str)

# Calculate precision, recall, and F1-score
expected_sentiments = data['Expected']
predicted_sentiments = data['Predicted']

#expected_sentiments = ['Positive', 'Positive', 'Positive', 'Neutral', 'Positive', 'Neutral', 'Positive', 'Positive', 'Positive', 'Positive', 'Positive', 'Positive', 'Positive', 'Positive', 'Negative', 'Negative', 'Negative', 'Negative', 'Negative', 'Negative', 'Negative', 'Negative']
#predicted_sentiments = ['Positive', 'Positive', 'Positive', 'Positive', 'Positive', 'Positive', 'Positive', 'Positive', 'Negative', 'Positive', 'Positive', 'Negative', 'Positive', 'Positive', 'Neutral', 'Negative', 'Negative', 'Negative', 'Negative', 'Negative', 'Negative', 'Neutral']

accuracy = accuracy_score(expected_sentiments, predicted_sentiments)
precision = precision_score(expected_sentiments, predicted_sentiments, average='weighted')
recall = recall_score(expected_sentiments, predicted_sentiments, average='weighted')
f1 = f1_score(expected_sentiments, predicted_sentiments, average='weighted')

# Print precision, recall, and F1-score
print(f"Accuracy {accuracy}")
print(f"Precision: {precision}")
print(f"Recall: {recall}")
print(f"F1-score: {f1}")

'''label_encoder = LabelEncoder()
expected_numeric = label_encoder.fit_transform(expected_sentiments)
predicted_numeric = label_encoder.transform(predicted_sentiments)

# Calculate precision score
precision = precision_score(expected_numeric, predicted_numeric, average='weighted')

precision = precision_score(expected_sentiments, predicted_sentiments, average='weighted')
recall = recall_score(expected_sentiments, predicted_sentiments, average='weighted')
f1 = f1_score(expected_sentiments, predicted_sentiments, average='weighted')

# Print precision, recall, and F1-score
print(f"Precision: {precision}")
print(f"Recall: {recall}")
print(f"F1-score: {f1}")'''

# Save the DataFrame with predicted sentiments appended to a new CSV file
