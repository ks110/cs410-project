import pandas as pd
import matplotlib.pyplot as plt

data = pd.read_csv('test_with_predictions_svm_updated.csv')

def categorize_release_year(year):
    return f"{(year // 10) * 10}-{((year // 10) * 10) + 9}"

data['Predicted_Year_Range'] = data['Release Year'].apply(categorize_release_year)

year_predicted = data.groupby('Predicted')['Predicted_Year_Range'].value_counts().unstack().fillna(0)

fig, axes = plt.subplots(figsize=(8, 6))

year_predicted.plot(kind='bar', stacked=True, ax=axes)
axes.set_title('Release Year with Predicted (10-year range)')
axes.set_ylabel('Count')
axes.legend(title='Predicted')

plt.tight_layout()
plt.show()
