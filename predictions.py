import pandas as pd
import matplotlib.pyplot as plt

data = pd.read_csv('test_with_predictions_svm_updated.csv')

genre_expected = data.groupby('Expected')['Genre'].value_counts().unstack().fillna(0)
genre_predicted = data.groupby('Predicted')['Genre'].value_counts().unstack().fillna(0)

def categorize_release_year(year):
    return f"{(year // 10) * 10}-{((year // 10) * 10) + 9}"

data['Expected_Year_Range'] = data['Release Year'].apply(categorize_release_year)
data['Predicted_Year_Range'] = data['Release Year'].apply(categorize_release_year)

year_expected = data.groupby('Expected')['Expected_Year_Range'].value_counts().unstack().fillna(0)
year_predicted = data.groupby('Predicted')['Predicted_Year_Range'].value_counts().unstack().fillna(0)

fig, axes = plt.subplots(2, 2, figsize=(12, 10))

# Plot for Genre with Expected and Predicted
genre_expected.plot(kind='bar', stacked=True, ax=axes[0, 0])
axes[0, 0].set_title('Genre with Expected')
axes[0, 0].set_ylabel('Count')
axes[0, 0].legend(title='Expected')

genre_predicted.plot(kind='bar', stacked=True, ax=axes[0, 1])
axes[0, 1].set_title('Genre with Predicted')
axes[0, 1].set_ylabel('Count')
axes[0, 1].legend(title='Predicted')

year_expected.plot(kind='bar', stacked=True, ax=axes[1, 0])
axes[1, 0].set_title('Release Year with Expected (10-year range)')
axes[1, 0].set_ylabel('Count')
axes[1, 0].legend(title='Expected')

year_predicted.plot(kind='bar', stacked=True, ax=axes[1, 1])
axes[1, 1].set_title('Release Year with Predicted (10-year range)')
axes[1, 1].set_ylabel('Count')
axes[1, 1].legend(title='Predicted')

plt.tight_layout()
plt.show()
