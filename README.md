# CS410-project
Implemented a simple movie review and recommendation system using sentiment analysis focusing on the reviews data collected and evaluations.

1. File considered: criticreviews.csv
2. Extracted the description column alone using description.py saved to descriptionreviewsonly.csv
and did sentiment analysis using sentimentana.py and saved to sentimentondescriptionreviewsonly.csv
3.  Merged sentimentondescriptionreviewsonly and criticreviews -> criticsreviewwithsentiment.csv

criticsreviewwithsentiment should be the file that needs to be considered for the next steps.  

MLTraining.py -> runs the sentiment analysis and model training using TFIDF and Naive-Bayes
SVMUpdate.py -> runs the sentiment analysis and model training using TFIDF and SVM.

test_with_predictions_svm.py -> used as the file for the top5movies.py

test.csv -> test data generated using chatGPT.

predictions*.py -> generating graph

Flow for running: any file can be executed independently, as we have the data in the repo. 

Else, 
sentimentana.py -> MLTraining.py/SVMUpdate.py -> top5movies.py