import pandas as pd
import tkinter as tk
from tkinter import ttk, messagebox

data = pd.read_csv('test_with_predictions_svm_updated.csv')

def top_4_movies_by_genre(genre):
    genre_data = data[data['Genre'] == genre]
    sorted_genre_data = genre_data.sort_values(by='Predicted', ascending=False)
    top_4_movies = sorted_genre_data.head(5)
    return top_4_movies[['Movie Name', 'Release Year']]

def show_top_movies():
    genre_req = genre_entry.get().strip()
    if genre_req:
        top_movies = top_4_movies_by_genre(genre_req)
        if not top_movies.empty:
            display_table(top_movies)
        else:
            messagebox.showinfo("Info", f"No movies found for genre: {genre_req}")
    else:
        messagebox.showwarning("Warning", "Please enter a movie genre.")

def display_table(top_movies):
    if 'tree' in locals():
        tree.destroy()
    
    tree = ttk.Treeview(root, columns=('Movie Name', 'Release Year'), show='headings')
    tree.heading('Movie Name', text='Movie Name')
    tree.heading('Release Year', text='Release Year')
    
    for index, row in top_movies.iterrows():
        tree.insert("", tk.END, values=(row['Movie Name'], row['Release Year']))
    
    tree.pack()

# Create GUI window
root = tk.Tk()
root.title("Top Movies by Genre")

# Create label and entry for user input
genre_label = tk.Label(root, text="Enter a movie genre:")
genre_label.pack()
genre_entry = tk.Entry(root)
genre_entry.pack()

# Create button to trigger the function
show_button = tk.Button(root, text="Show Top Movies", command=show_top_movies)
show_button.pack()

root.mainloop()
