import pandas as pd
import nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from sklearn.calibration import LabelEncoder
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score
from sklearn.model_selection import GridSearchCV

nltk.download('punkt')
nltk.download('stopwords')

def preprocess_text(text):
    tokens = word_tokenize(text.lower())  
    
    stop_words = set(stopwords.words('english'))
    filtered_tokens = [word for word in tokens if word.isalnum() and word not in stop_words]
    
    return ' '.join(filtered_tokens)

df = pd.read_csv('criticsreviewwithsentiment.csv', encoding='utf-8')

df['preprocessed_summary'] = df['summary'].fillna('').apply(preprocess_text)

X = df['preprocessed_summary']
y = df['Sentiment']

tfidf_vectorizer = TfidfVectorizer()
X_tfidf = tfidf_vectorizer.fit_transform(X)

param_grid = {'C': [0.1, 1, 10], 'kernel': ['linear', 'rbf', 'poly']}
grid_search = GridSearchCV(SVC(), param_grid, cv=5)
grid_search.fit(X_tfidf, y)

best_params = grid_search.best_params_
best_estimator = grid_search.best_estimator_

best_estimator.fit(X_tfidf, y)

data = pd.read_csv("test.csv")
test_data = data['Review'].fillna('').tolist()

predicted_sentiments = []  
for review in test_data:
    preprocessed_review = preprocess_text(review)
    review_tfidf = tfidf_vectorizer.transform([preprocessed_review])
    prediction = best_estimator.predict(review_tfidf)
    predicted_sentiments.append(prediction[0])  
    print(f"Review: {review} | Predicted Sentiment: {prediction[0]}")

data['Predicted'] = predicted_sentiments
data.to_csv('test_with_predictions_svm_updated.csv', index=False, encoding='utf-8')

# Calculate precision, recall, and F1-score with the updated model
expected_sentiments = data['Expected']
predicted_sentiments = data['Predicted']

accuracy = accuracy_score(expected_sentiments, predicted_sentiments)
precision = precision_score(expected_sentiments, predicted_sentiments, average='weighted')
recall = recall_score(expected_sentiments, predicted_sentiments, average='weighted')
f1 = f1_score(expected_sentiments, predicted_sentiments, average='weighted')

# Print precision, recall, and F1-score
print(f"Accuracy: {accuracy}")
print(f"Precision: {precision}")
print(f"Recall: {recall}")
print(f"F1-score: {f1}")
