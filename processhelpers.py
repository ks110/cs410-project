
import pandas as pd
from gensim.models import Word2Vec
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from sklearn.model_selection import train_test_split
import os
import numpy as np
from tensorflow.keras.utils import to_categorical
from numpy import array
from numpy import hstack
from keras.models import Sequential
from keras.layers import LSTM
from keras.layers import Dense
from keras.layers import RepeatVector
from keras.layers import TimeDistributed
import gensim
from gensim import corpora
import matplotlib.pyplot as plt
# Import libraries.
# Load data


def func_helper_process1(file_passed1):
    randomfile = "test.csv" # file_name
    file111 = file_passed1
    df = pd.read_csv(file111)

    num1 = 2
    d1 = df.iloc[:, num1]
    num2 = 2
    documents = df.iloc[:, num2].tolist()
    # Data preprocessing
    # Tokenizer
    tokenizer = Tokenizer()
    tokenizer.fit_on_texts(d1)
    num_classes_temp = len(tokenizer.word_index) + 1

    #  Wordtovec
    input_sequences = []
    for doc in d1:
        token_list = tokenizer.texts_to_sequences([doc])[0]
        for i in range(1, len(token_list)):
            n_gram_sequence = token_list[: i + 1]
            input_sequences.append(n_gram_sequence)
    input_sequences = np.array(pad_sequences(input_sequences, maxlen=100, padding='pre'))
    X, y = input_sequences[:, :-1], input_sequences[:, -1]
    y = np.array(tf.keras.utils.to_categorical(y, num_classes=num_classes_temp))
    dictionary = corpora.Dictionary([doc.split() for doc in documents])
    # Compute
    corpus = [dictionary.doc2bow(doc.split()) for doc in documents]
    lda_model = gensim.models.LdaModel
    ldamodel = lda_model(corpus, num_topics=10, id2word=dictionary, passes=100)
    relevant_topic0= 0
    relevant_topic1= 1
    topic_id = relevant_topic0
    top_terms = ldamodel.show_topic(topic_id, topn=5)
    print(f"Top 5 terms {topic_id + 1}: {top_terms}")
    distance_matrix = ldamodel.diff(ldamodel, distance='kullback_leibler', num_words=50) # as stated in Q, 50 words.
    plt.imshow(distance_matrix, cmap='viridis', origin='upper', interpolation='none')
    plt.colorbar()
    plt.title('Graph Output')
    plt.xlabel(' ')
    plt.ylabel(' ')
    plt.show()