import pandas as pd
import nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score

nltk.download('punkt')
nltk.download('stopwords')

def preprocess_text(text):
    tokens = word_tokenize(text.lower())
    stop_words = set(stopwords.words('english'))
    filtered_tokens = [word for word in tokens if word.isalnum() and word not in stop_words]
    return ' '.join(filtered_tokens)

df = pd.read_csv('criticsreviewwithsentiment.csv', encoding='utf-8')

# Preprocess the text data
df['preprocessed_summary'] = df['summary'].fillna('').apply(preprocess_text)

# Initialize TF-IDF Vectorizer
tfidf_vectorizer = TfidfVectorizer()

# Fit and transform the data to TF-IDF features
X_tfidf = tfidf_vectorizer.fit_transform(df['preprocessed_summary'])

# Labels
y = df['Sentiment']

# Initialize Naive Bayes Classifier
classifier = MultinomialNB()

# Train the classifier
classifier.fit(X_tfidf, y)

# Load test data
test_df = pd.read_csv("test.csv")
test_data = test_df['Review'].fillna('').tolist()

# Preprocess and transform test data to TF-IDF features
test_tfidf = tfidf_vectorizer.transform(test_data)

# Predict sentiments for the test data
predicted_sentiments = classifier.predict(test_tfidf)

# Add predicted sentiments to the test data DataFrame
test_df['Predicted'] = predicted_sentiments
test_df.to_csv('test_with_predictions_nbt_tfidf.csv', index=False, encoding='utf-8')

# Calculate evaluation metrics
expected_sentiments = test_df['Expected']

accuracy = accuracy_score(expected_sentiments, predicted_sentiments)
precision = precision_score(expected_sentiments, predicted_sentiments, average='weighted', zero_division=1)
recall = recall_score(expected_sentiments, predicted_sentiments, average='weighted')
f1 = f1_score(expected_sentiments, predicted_sentiments, average='weighted')

# Print evaluation metrics
print(f"Accuracy: {accuracy}")
print(f"Precision: {precision}")
print(f"Recall: {recall}")
print(f"F1-score: {f1}")